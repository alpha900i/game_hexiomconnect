#ifndef WORKER_H
#define WORKER_H

#include "hexmap.h"
#include <QObject>
#include <QThread>

class WorkerThread : public QThread
{
    Q_OBJECT

    QObject* myParent;
    HexMap* myMap;
public:
    WorkerThread(QObject *parent,HexMap* map);

public slots:
    void run();

signals:
    void resultReady(const QString &result);
};

#endif // WORKER_H
