#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "worker.h"

#include <ctime>

#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QFileDialog>
#include <QStyle>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    scene = new QGraphicsScene();
    initMap();
    initMenu();

    ui->graphicsView->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::initMenu()
{
    connect(this,&QAction::destroyed,this,&MainWindow::myClose);

    //делаем отдельные действия для меню
    //открыть файл
    QAction *openFileAction = new QAction(tr("Open file"),this);
    openFileAction->setShortcut(QKeySequence(tr("Ctrl+O")));
    openFileAction->setIcon(style()->standardIcon(QStyle::SP_DirOpenIcon));
    connect(openFileAction, &QAction::triggered, this, &MainWindow::openFile);

    //вынести лишнее с карты в ведро
    QAction *cutAction = new QAction(tr("&Cut loose"), this);
    cutAction->setShortcut(QKeySequence(tr("Ctrl+Shift+L")));
    cutAction->setIcon(QIcon(tr("..\\HexiomConnect\\cut.ico")));
    connect(cutAction, &QAction::triggered, this, &MainWindow::cutLoose);


    //одношаговая проверка однозначных действий (установка элементов, которые больше некуда и
    //заполнение узлов, которые больше нечем)
    QAction *stepAction = new QAction(tr("&Singleout"), this);
    stepAction->setShortcut(QKeySequence(tr("Ctrl+Shift+S")));
    stepAction->setIcon(QIcon(tr("..\\HexiomConnect\\one.ico")));
    connect(stepAction, &QAction::triggered, this, &MainWindow::singleOut);


    //рекурсия "по ведру" (для каждого элемента ведра перебираем, куда его лучше поставить)
    QAction *bucketRecursionAction = new QAction(tr("Recursive by bucket"), this);
    bucketRecursionAction->setShortcut(QKeySequence(tr("Ctrl+Shift+B")));
    bucketRecursionAction->setIcon(QIcon(tr("..\\HexiomConnect\\bucket.ico")));
    connect(bucketRecursionAction, &QAction::triggered, this, &MainWindow::bucketRecursion);


    //рекурсия "по карте" (для каждого узла карты перебираем, кого сюда лучше поставить)
    QAction *mapRecursionAction = new QAction(tr("Recursive by map"), this);
    mapRecursionAction->setShortcut(QKeySequence(tr("Ctrl+Shift+M")));
    mapRecursionAction->setIcon(QIcon(tr("..\\HexiomConnect\\map.ico")));
    connect(mapRecursionAction, &QAction::triggered, this, &MainWindow::mapRecursion);



    //формируем меню как таковое
    QMenu* mainMenu = ui->menuBar->addMenu("Main");
    mainMenu->addAction(openFileAction);
    mainMenu->addSeparator();
    mainMenu->addAction(cutAction);
    mainMenu->addAction(stepAction);
    mainMenu->addAction(bucketRecursionAction);
    mainMenu->addAction(mapRecursionAction);

    ui->toolBar->addAction(openFileAction);
    ui->toolBar->addSeparator();
    ui->toolBar->addAction(cutAction);
    ui->toolBar->addAction(stepAction);
    ui->toolBar->addAction(bucketRecursionAction);
    ui->toolBar->addAction(mapRecursionAction);
}
void MainWindow::openFile()
{
    QFileDialog dialog(this);
    dialog.setOptions(QFileDialog::DontUseNativeDialog);
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setNameFilter(tr("Text files (*.txt)"));
    dialog.setViewMode(QFileDialog::Detail);
    dialog.setDirectory(QDir("..\\HexiomConnect\\tests\\"));
    QStringList fileNames;
    if (!dialog.exec())
        return;

    fileNames = dialog.selectedFiles();
    QString fileName=fileNames[0];

    delete map;
    map=new HexMap();
    if(map->readFile(fileName))
    {
        QMessageBox msg;
        msg.setText("Parse succesful!");
        msg.exec();
    }
    else
    {
        QMessageBox msg;
        msg.setText("Parse error!");
        msg.exec();
        this->setWindowTitle("HexiomConnectSolver – Error while parsing "+fileName);
        return;
    }

    this->setWindowTitle("HexiomConnectSolver – "+fileName);
    draw();
}
void MainWindow::myClose()
{
    delete map;
}

void MainWindow::initMap()
{
    map=new HexMap();
    //openFile();
}
void MainWindow::cutLoose()
{
    map->cutLoose();
    draw();
}
void MainWindow::singleOut()
{
    int resultBucket,resultMap;
    do
    {
        resultBucket=map->checkBucket();
        resultMap=map->checkMap();
        if (resultBucket==-1 || resultMap==-1)
        {
            return;
        }
    }while(resultBucket==1 || resultMap==1);

    draw();
}
void MainWindow::draw()
{
    delete scene;
    scene = new QGraphicsScene();
    map->draw(scene);
    ui->graphicsView->setScene(scene);
}
void MainWindow::bucketRecursion()
{
    time_t start=clock();
    map->bucketRecursion(0);
    time_t finish=clock();
    int timeSpent=(finish-start)/1000;
    QMessageBox msg;
    msg.setText(QString("Bucket recursion done! %1 seconds spent").arg(timeSpent));
    msg.exec();
    draw();
}
void MainWindow::mapRecursion()
{
    time_t start=clock();

    map->mapRecursion(0);

    /*WorkerThread* mapWorkerThread=new WorkerThread(this,map);
    connect(mapWorkerThread, &WorkerThread::resultReady, this, &MainWindow::handleResults);
    connect(mapWorkerThread, &WorkerThread::finished, mapWorkerThread, &QObject::deleteLater);
    mapWorkerThread->start();*/

    time_t finish=clock();
    int timeSpent=(finish-start)/1000;
    QMessageBox msg;
    msg.setText(QString("Map recursion done! %1 seconds spent").arg(timeSpent));
    msg.exec();
    draw();
}


void MainWindow::handleResults()
{
    QMessageBox msg;
    msg.setText(QString("Map recursion done! %1 seconds spent").arg(0));
    msg.exec();
    draw();
}
