#include "hex.h"

Hex::Hex()
{
    real=false;
    fixed=false;
    for(int i=0;i<SIDECOUNT;i++)
        sides[i]=NONE;
}

void Hex::setSide(int index,color side)
{
    this->sides[index]=side;
}
bool Hex::setSide(int index, QString side)
{
    QString lowerSide=side.toLower();
    if (lowerSide=="blue")
    {
        setSide(index,BLUE);
        return true;
    }
    if (lowerSide=="red")
    {
        setSide(index,RED);
        return true;
    }
    if (lowerSide=="yellow")
    {
        setSide(index,YELLOW);
        return true;
    }
    if (lowerSide=="green")
    {
        setSide(index,GREEN);
        return true;
    }
    if (lowerSide=="none")
    {
        setSide(index,NONE);
        return true;
    }
    setSide(index,ERROR);
    return false;
}
