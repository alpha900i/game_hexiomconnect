#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>

#include "hexmap.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    HexMap* map;
    QGraphicsScene* scene;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void openFile();
    void myClose();

    void initMenu();
    void initMap();

    void cutLoose();
    void singleOut();
    void bucketRecursion();
    void mapRecursion();

    void draw();

    void handleResults();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
