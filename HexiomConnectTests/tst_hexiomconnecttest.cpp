#include <QtTest>
#include <QCoreApplication>

// add necessary includes here
#include "../hexmap.h"

class HexiomConnectTest : public QObject
{
    Q_OBJECT

public:
    HexiomConnectTest();
    ~HexiomConnectTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void readMap();

};

HexiomConnectTest::HexiomConnectTest()
{

}

HexiomConnectTest::~HexiomConnectTest()
{

}

void HexiomConnectTest::initTestCase()
{

}

void HexiomConnectTest::cleanupTestCase()
{

}

void HexiomConnectTest::readMap()
{
    HexMap hexMap;
    bool status=hexMap.readFile("..\\HexiomConnectTests\\tests\\39q.txt");
    QCOMPARE(status,true);
}


QTEST_MAIN(HexiomConnectTest)

#include "tst_hexiomconnecttest.moc"
