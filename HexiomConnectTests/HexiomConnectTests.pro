QT += testlib gui widgets
CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

SOURCES +=  tst_hexiomconnecttest.cpp \
    ../hex.cpp \
    ../hexgraphics.cpp \
    ../hexmap.cpp \
    ../mainwindow.cpp \
    ../worker.cpp

HEADERS += \
    ../hex.h \
    ../hexgraphics.h \
    ../hexmap.h \
    ../mainwindow.h \
    ../ui_mainwindow.h \
    ../worker.h
