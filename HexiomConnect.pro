#-------------------------------------------------
#
# Project created by QtCreator 2018-06-22T22:11:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HexiomConnect
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    hex.cpp \
    hexgraphics.cpp \
    hexmap.cpp \
    worker.cpp

HEADERS += \
    hex.h \
    hexgraphics.h \
    hexmap.h \
    mainwindow.h \
    worker.h

FORMS += \
        mainwindow.ui

DISTFILES += \
    input.txt \
    tests/4.txt \
    tests/32.txt \
    tests/33.txt \
    tests/34.txt \
    tests/34a.txt \
    tests/35.txt \
    tests/36.txt \
    tests/37.txt \
    tests/38.txt \
    tests/39.txt \
    tests/40.txt \
    log.txt

win32 {
    RC_FILE = hexiomconnect.rc
}
