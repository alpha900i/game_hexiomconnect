#include "hexmap.h"

#include <QMessageBox>
#include <QApplication>

HexMap::HexMap()
{
    mapWidth=0;
    mapHeigth=0;

    anyHex.fixed=false;
    anyHex.real=true;
    for(int i=0;i<Hex::SIDECOUNT;i++)
        anyHex.setSide(i,Hex::color::ANY);


    outputFile.setFileName("..\\HexiomConnect\\log.txt");
    if (!outputFile.open(QIODevice::WriteOnly|QIODevice::Truncate)) //TODO обработка ошибки
    {
        ;
    }
    out.setDevice(&outputFile);
    out << "Log started!\r\n";
}
HexMap::~HexMap()
{
    bucket.clear();
    if (mapWidth && mapHeigth)
    {
        for(int i=0;i<mapWidth;i++)
            delete[] map[i];
        delete[] map;
    }
    outputFile.close();
}

bool HexMap::readFile(QString fileName)
{
    int realCount;
    int curX,curY;

    QString curSides;
    QStringList curSidesList;


    QFile inputFile(fileName);
    if (!inputFile.open(QIODevice::ReadOnly)) //TODO обработка ошибки
    {
        ;
    }
    QTextStream in(&inputFile);

    bucket.clear();

    //сначала по размерам карты генерируем карту. С большим запасом, ибо гексы
    in >> mapWidth >> mapHeigth;
    if (in.status()!=QTextStream::Ok)
    {
        out << "Error while reading map size!\r\n" << endl;
        return false;
    }

    map=new Hex*[mapWidth];
    for(int i=0;i<mapWidth;i++)
        map[i]=new Hex[mapHeigth];

    //реально нам понадобится лишь часть гексов. Кстати, сколько?
    in >> realCount;
    if (in.status()!=QTextStream::Ok)
    {
        out << "Error while reading real count!\r\n" << endl;
        return false;
    }
    //считываем их окрас, помечаем их реальными
    for(int i=0;i<realCount;i++)
    {
        //читаем и всячески проверяем координаты гекса
        in >> curX >> curY;
        if (in.status()!=QTextStream::Ok)
        {
            out << "Error while reading hex " << i+1 << " coordinates!\r\n" << endl;
            return false;
        }
        if (curX>=mapWidth || curY>=mapHeigth)
        {
            out << "Coordinates for hex " << i+1 << " exceed map size!\r\n" << endl;
            return false;
        }
        if (map[curX][curY].real)
        {
            out << "Hex " << i+1 << " use already existing coordinates!\r\n" << endl;
            return false;
        }

        //гекс реален
        map[curX][curY].real=true;

        //считываем и парсим перечень сторон
        curSides=in.readLine();
        curSidesList=curSides.trimmed().split(" ");

        if(curSidesList.size()!=6 && curSidesList.size()!=7)
        {
            out << "Hex " << i+1 << " have wrong amount of sides!\r\n" << endl;
            return false;
        }

        for(int j=0;j<Hex::SIDECOUNT;j++)
        {
            if(!map[curX][curY].setSide(j,curSidesList[j]))
            {
                out << "Hex " << i+1 << " have wrong data on " << j+1 << " pos!\r\n" << endl;
                return false;
            }
        }

        //7 сторон? либо фиксированный гекс, либо ошибка
        if (curSidesList.size()==7)
        {
            //фиксированные гексы просто кладем на карту
            if(curSidesList[6].toLower()=="fixed")
            {
                map[curX][curY].fixed=true;
            }
            else
            {
                out << "Hex " << i+1 << " have wrong data on 7th pos!\r\n" << endl;
                return false;
            }
        }
        else
        {
            //нефиксированные гексы кладем на карту и в ведро
            bucket.push_back(map[curX][curY]);
            map[curX][curY]=anyHex;
        }

    }
    out << "Parse successful!" << endl;
    return true;
}
void HexMap::draw(QGraphicsScene* scene)
{
    drawMap(scene);
    drawBucket(scene);
}
//рисуем игровое поле
void HexMap::drawMap(QGraphicsScene* scene)
{
    double startX=60,startY=60;
    double curX,curY;
    curY=startY;

    HexGraphics* curGraphics;

    for(int i=0;i<mapWidth;i++)
    {
        curX=startX;
        if(i%2)
            curX+=1.5*HexGraphics::RADIUS;
        for(int j=0;j<mapHeigth;j++)
        {
            curGraphics=new HexGraphics(&map[i][j],curX,curY);
            scene->addItem(curGraphics);
            curX+=3*HexGraphics::RADIUS;
        }
        curY+=HexGraphics::R32;
    }
    scene->addRect(QRectF(59,59,2,2));
}
//рисуем ведро с неиспользованными элементами
void HexMap::drawBucket(QGraphicsScene* scene)
{
    double startX=60,startY=-60;
    double curX=startX,curY=startY;
    int maxItemsInLine=20;

    HexGraphics* curGraphics;

    for(int i=0,curItems=0;i<bucket.size();i++,curItems++)
    {
        if (curItems>maxItemsInLine)
        {
            curItems=0;
            curX=startX;
            curY-=2*HexGraphics::RADIUS;
        }
        curGraphics=new HexGraphics(&bucket[i],curX,curY);
        scene->addItem(curGraphics);
        curX+=2*HexGraphics::RADIUS;
    }

}
void HexMap::cutLoose()
{
    bucket.clear();
    for(int i=0;i<mapWidth;i++)
        for(int j=0;j<mapHeigth;j++)
            if(map[i][j].real && !map[i][j].fixed)
            {
                bucket.push_back(map[i][j]);
                map[i][j]=anyHex;
            }
}
bool HexMap::isFit(Hex candidate, int x, int y)
{
    if(map[x][y].fixed)
        return false;

    bool isOdd=(x%2!=0);
    int shifts[Hex::SIDECOUNT][2];
    for (int i=0;i<Hex::SIDECOUNT;i++)
        for(int j=0;j<2;j++)
            if(isOdd)
                shifts[i][j]=oddShifts[i][j];
            else
                shifts[i][j]=evenShifts[i][j];

    int checkX,checkY,checkSide;
    Hex::color checkedColor;
    for(int i=0;i<Hex::SIDECOUNT;i++)
    {
        checkX=x+shifts[i][0];
        checkY=y+shifts[i][1];
        checkSide=(i+Hex::SIDECOUNT/2)%Hex::SIDECOUNT;
        checkedColor=getSideByIndex(checkX,checkY,checkSide);
        if (checkedColor!=Hex::color::ANY)
            if (checkedColor!=candidate.sides[i])
                return false;
    }
    return true;
}
Hex::color HexMap::getSideByIndex(int x, int y, int side)
{
    if ((x<0)||(x>=mapWidth))
        return Hex::color::NONE;
    if ((y<0)||(y>=mapHeigth))
        return Hex::color::NONE;
    if (!map[x][y].real)
        return Hex::color::NONE;
    return map[x][y].sides[side];
}
//ищем в ведре гексы, для которых есть единственное возможное положение
int HexMap::checkBucket()
{
    if(!bucket.size())
        return HexMap::checkResult::SUCCESS;

    int checkResult=HexMap::checkResult::NOTHING;
    int foundCount;

    //изначальные значения -1 пригодиться не должны
    //но а) пусть компилятор не ноет, что их могут использовать без инициализации
    //б) если их все же каким-то чудом используют, пусть падает нафиг
    int lastX=-1,lastY=-1;

    //сохраняем оригинальный расклад, чтоб восстановить при противоречии
    Hex** tempMap=new Hex*[mapWidth];
    for(int i=0;i<mapWidth;i++)
    {
        tempMap[i]=new Hex[mapHeigth];
        for(int j=0;j<mapHeigth;j++)
            tempMap[i][j]=map[i][j];
    }
    QVector<Hex> tempBucket;
    tempBucket.resize(bucket.size());
    for(int i=0;i<bucket.size();i++)
        tempBucket[i]=bucket[i];


    //каждый гекс в ведре пытаемся упихать в карту. Считаем число допустимых позиций.
    for(int i=0;i<bucket.size();i++)
    {
        foundCount=0;
        for(int j=0;j<mapWidth;j++)
            for(int k=0;k<mapHeigth;k++)
            {
                if(!map[j][k].real)
                    continue;
                if(map[j][k].fixed)
                    continue;

                if(isFit(bucket[i],j,k))
                {
                    foundCount++;
                    lastX=j;
                    lastY=k;
                }
            }
        //вообще не сходится
        if (foundCount==0)
        {
            //out << "Check bucket found contradiction" << endl;
            QMessageBox msg;
            //msg.setText(QString("Contradiction! No place for hex %1").arg(i));
            //msg.exec();

            //откатываем сделаное
            for(int i1=0;i1<mapWidth;i1++)
            {
                for(int j1=0;j1<mapHeigth;j1++)
                    map[i1][j1]=tempMap[i1][j1];
                delete[] tempMap[i1];
            }
            delete[] tempMap;
            bucket.clear();
            bucket.resize(tempBucket.size());
            for(int i1=0;i1<tempBucket.size();i1++)
                bucket[i1]=tempBucket[i1];
            tempBucket.clear();

            return HexMap::checkResult::CONTRADICT;
        }
        //уникальный гекс. Пихаем в единственное возможное.
        if (foundCount==1)
        {
            //out << "Check bucket found unique: [" <<lastX<<"]["<<lastY<<"] <=" << i << endl;

            map[lastX][lastY]=bucket[i];
            map[lastX][lastY].fixed=true;
            bucket.erase(bucket.begin()+i);
            i--;
            checkResult=HexMap::checkResult::SUCCESS;
        }
    }


    //освобождаем временно выделенную память
    for(int i1=0;i1<mapWidth;i1++)
        delete[] tempMap[i1];
    delete[] tempMap;
    tempBucket.clear();

    return checkResult;
}
//ищем слоты, для которых допустим единственный гекс
int HexMap::checkMap()
{
    if(!bucket.size())
        return HexMap::checkResult::SUCCESS;

    int checkResult=HexMap::checkResult::NOTHING;
    int foundCount;

    //изначальные значения -1 пригодиться не должны
    //но а) пусть компилятор не ноет, что их могут использовать без инициализации
    //б) если их все же каким-то чудом используют, пусть падает нафиг
    int lastI=-1;

    //сохраняем оригинальный расклад, чтоб восстановить при противоречии
    Hex** tempMap=new Hex*[mapWidth];
    for(int i=0;i<mapWidth;i++)
    {
        tempMap[i]=new Hex[mapHeigth];
        for(int j=0;j<mapHeigth;j++)
            tempMap[i][j]=map[i][j];
    }
    QVector<Hex> tempBucket;
    tempBucket.resize(bucket.size());
    for(int i=0;i<bucket.size();i++)
        tempBucket[i]=bucket[i];

    for(int j=0;j<mapWidth;j++)
        for(int k=0;k<mapHeigth;k++)
        {
            if(!map[j][k].real)
                continue;
            if(map[j][k].fixed)
                continue;

            foundCount=0;
            for(int i=0;i<bucket.size();i++)
                if(isFit(bucket[i],j,k))
                {
                    foundCount++;
                    lastI=i;
                }

            //вообще не сходится
            if (foundCount==0)
            {
                QMessageBox msg;

                //откатываем сделаное
                for(int i1=0;i1<mapWidth;i1++)
                {
                    for(int j1=0;j1<mapHeigth;j1++)
                        map[i1][j1]=tempMap[i1][j1];
                    delete[] tempMap[i1];
                }
                delete[] tempMap;

                bucket.clear();
                bucket.resize(tempBucket.size());
                for(int i1=0;i1<tempBucket.size();i1++)
                    bucket[i1]=tempBucket[i1];
                tempBucket.clear();

                return HexMap::checkResult::CONTRADICT;
            }
            //уникальное предложение. Пихаем в единственный возможный гекс.
            if (foundCount==1)
            {
                //out << "Check map found unique: [" <<j<<"]["<<k<<"] <=" << lastI << endl;
                map[j][k]=bucket[lastI];
                map[j][k].fixed=true;
                bucket.erase(bucket.begin()+lastI);
                checkResult=HexMap::checkResult::SUCCESS;
            }
        }

    //освобождаем временно выделенную память
    for(int i1=0;i1<mapWidth;i1++)
        delete[] tempMap[i1];
    delete[] tempMap;
    tempBucket.clear();

    return checkResult;
}


int HexMap::checkWithRollback()
{
    //массив для оригинального расклада, чтоб восстановить при противоречии
    Hex** tempMap;
    QVector<Hex> tempBucket;


    //выделяем временные массивы
    tempMap=new Hex*[mapWidth];
    for(int i1=0;i1<mapWidth;i1++)
        tempMap[i1]=new Hex[mapHeigth];
    tempBucket.resize(bucket.size());

    //сохраняем оригинальный расклад, чтоб восстановить при противоречии
    for(int i1=0;i1<mapWidth;i1++)
        for(int j1=0;j1<mapHeigth;j1++)
            tempMap[i1][j1]=map[i1][j1];
    for(int i1=0;i1<bucket.size();i1++)
        tempBucket[i1]=bucket[i1];


    //гоняем проверку по карте и проверку по ведру,  пока из ведра хоть что-то изымается
    //удачей считаем только ситуации, когда удалось поставить ВСЕХ
    //одиночек мы так или иначе вытащим по рекурсии в основной рекурсии
    //а так проще откатывать и экономнее по памяти
    //делать откаты в рекурсивных функциях смерти подобно - темповые массивы копятся
    int bucketSize;
    int result;
    do
    {
        bucketSize=bucket.size();
        result=checkMap();
        if (result==HexMap::checkResult::CONTRADICT)
            break;
        result=checkBucket();
        if (result==HexMap::checkResult::CONTRADICT)
            break;

    }while(bucketSize!=bucket.size());

    //ведро не кончилось? мы не выиграли!
    //откатываем сделанное.
    if (bucketSize)
    {
        for(int i1=0;i1<mapWidth;i1++)
            for(int j1=0;j1<mapHeigth;j1++)
                map[i1][j1]=tempMap[i1][j1];
        bucket.clear();
        bucket.resize(tempBucket.size());
        for(int i1=0;i1<tempBucket.size();i1++)
            bucket[i1]=tempBucket[i1];
    }

    //удаляем временные массивы
    for(int i1=0;i1<mapWidth;i1++)
        delete[] tempMap[i1];
    delete[] tempMap;
    tempBucket.clear();

    //что-то надо вернуть...
    if (!bucketSize)
        return HexMap::checkResult::SUCCESS;
    if (result==HexMap::checkResult::CONTRADICT)
        return HexMap::checkResult::CONTRADICT;
    else
        return HexMap::checkResult::NOTHING;
}
//рекурсивный поиск с упором на карту
int HexMap::mapRecursion(int level)
{
    if(!bucket.size())
        return HexMap::checkResult::SUCCESS;

    int minHexes=bucket.size()+1;

    //изначальные значения -1 пригодиться не должны
    //но а) пусть компилятор не ноет, что их могут использовать без инициализации
    //б) если их все же каким-то чудом используют, пусть падает нафиг
    int minX=-1,minY=-1;
    int foundCount;

    //выбираем слот карты, для которого меньше всего допустимых гексов
    for(int j=0;j<mapWidth;j++)
        for(int k=0;k<mapHeigth;k++)
        {
            if(!map[j][k].real)
                continue;
            if(map[j][k].fixed)
                continue;

            foundCount=0;
            for(int i=0;i<bucket.size();i++)
                if(isFit(bucket[i],j,k))
                    foundCount++;

            if(foundCount<minHexes)
            {
                minHexes=foundCount;
                minX=j;
                minY=k;
            }
        }

    if (minHexes==0)
        return HexMap::checkResult::NOTHING;

    Hex temp;
    for(int i=0;i<bucket.size();i++)
        if(isFit(bucket[i],minX,minY))
        {
            temp=bucket[i];
            //ставим
            map[minX][minY]=temp;
            map[minX][minY].fixed=true;
            bucket.erase(bucket.begin()+i);
            //проверяем допустимость расклада
            //запускаем рекурсию
            if (checkWithRollback()!=HexMap::checkResult::CONTRADICT)
                if(mapRecursion(level+1)==HexMap::checkResult::SUCCESS)
                    return HexMap::checkResult::SUCCESS;

            //откатываем сделаное
            map[minX][minY]=anyHex;
            bucket.insert(bucket.begin()+i,temp);
        }

    return HexMap::checkResult::NOTHING;
}

//TODO
//рекурсивный поиск
//выбираем элемент ведра, для которого меньше всего допустимых позиций
//ставим
//проверяем допустимость расклада
//запускаем рекурсию
int HexMap::bucketRecursion(int level)
{
    if(!bucket.size())
        return HexMap::checkResult::SUCCESS;

    //минимальное число элементов, помещающихся в некое ведро
    int minHexes=bucket.size()+1;

    //изначальное значение -1 пригодиться не должно
    //но а) пусть компилятор не ноет, что его могут использовать без инициализации
    //б) если их все же каким-то чудом используют, пусть падает нафиг
    int minI=-1;
    int foundCount;

    for(int i=0;i<bucket.size();i++)
    {
        foundCount=0;
        for(int j=0;j<mapWidth;j++)
            for(int k=0;k<mapHeigth;k++)
            {
                if(!map[j][k].real)
                    continue;
                if(map[j][k].fixed)
                    continue;

                if(isFit(bucket[i],j,k))
                    foundCount++;
            }
        if(foundCount<minHexes)
        {
            minHexes=foundCount;
            minI=i;
        }
    }

    //если хоть для кого-то в ведре нет подходящего слота - решения нет
    if (minHexes==0)
        return HexMap::checkResult::NOTHING;

    Hex temp;
    for(int j=0;j<mapWidth;j++)
        for(int k=0;k<mapHeigth;k++)
        {
            if(!map[j][k].real)
                continue;
            if(map[j][k].fixed)
                continue;

            if(isFit(bucket[minI],j,k))
            {
                temp=bucket[minI];

                //ставим
                map[j][k]=temp;
                map[j][k].fixed=true;
                bucket.erase(bucket.begin()+minI);

                //проверяем допустимость расклада
                //запускаем рекурсию
                if (checkWithRollback()!=HexMap::checkResult::CONTRADICT)
                    if(bucketRecursion(level+1)==HexMap::checkResult::SUCCESS)
                        return HexMap::checkResult::SUCCESS;

                //откатываем сделаное
                map[j][k]=anyHex;
                bucket.insert(bucket.begin()+minI,temp);
            }
        }

    return HexMap::checkResult::NOTHING;
}
