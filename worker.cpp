#include "worker.h"

WorkerThread::WorkerThread(QObject *parent, HexMap *map)
{
    myParent=parent;
    myMap=map;
}

void WorkerThread::run()
{
    QString result;
    myMap->mapRecursion(0);
    emit resultReady("Done");
}
