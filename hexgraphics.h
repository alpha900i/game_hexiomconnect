#ifndef HEXGRAPHICS_H
#define HEXGRAPHICS_H

#include <QGraphicsItem>
#include <QPainter>
#include "hex.h"

class HexGraphics : public QGraphicsItem
{

    Hex* data;
    double centerX,centerY;
    QVector<QPointF> points;
    QPolygonF* me;
public:  
    static constexpr double RADIUS=20;
    static constexpr double SQRT3=1.7320508075688772935274463415059;
    static constexpr double R32=RADIUS/2*SQRT3;

    HexGraphics();
    HexGraphics(Hex* data, double cx, double cy);
    ~HexGraphics();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                            QWidget *widget);
};

#endif // HEXGRAPHICS_H
