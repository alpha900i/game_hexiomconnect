#ifndef HEX_H
#define HEX_H

#include <QString>

class Hex
{
public:
    enum color{NONE,RED,BLUE,YELLOW,GREEN,ANY,ERROR};
    enum sideName{UP,UPRIGHT,DOWNRIGHT,DOWN,DOWNLEFT,UPLEFT};
    static const int SIDECOUNT=6;
    color sides[SIDECOUNT];
    bool real;
    bool fixed;

    Hex();
    void setSide(int index,color side);
    bool setSide(int index, QString side);
};

#endif // HEX_H
