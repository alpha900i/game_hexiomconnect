#include "hexgraphics.h"

HexGraphics::HexGraphics()
{

}
HexGraphics::HexGraphics(Hex* data, double cx, double cy)
{
    this->data=data;
    this->centerX=cx;
    this->centerY=cy;

    points.push_back(QPointF(centerX-RADIUS/2,centerY-R32));
    points.push_back(QPointF(centerX+RADIUS/2,centerY-R32));
    points.push_back(QPointF(centerX+RADIUS,centerY));
    points.push_back(QPointF(centerX+RADIUS/2,centerY+R32));
    points.push_back(QPointF(centerX-RADIUS/2,centerY+R32));
    points.push_back(QPointF(centerX-RADIUS,centerY));

    me=new QPolygonF(points);
}
HexGraphics::~HexGraphics()
{
    delete me;
}

QRectF HexGraphics::boundingRect() const
{
    /*return QRectF(centerX-RADIUS, centerY-R32,
                  centerX+RADIUS, centerY+R32);*/
    return me->boundingRect();
}

void HexGraphics::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                        QWidget *widget)
{
    int index1,index2;
    QColor color;

    if(data->real)
    {
        if(data->fixed)
            painter->setBrush(QBrush(Qt::GlobalColor::gray));
        else
            painter->setBrush(QBrush(Qt::GlobalColor::white));
        painter->setPen(QPen(QBrush(Qt::GlobalColor::black),3));
        painter->drawPolygon(*me);
        for(int i=0;i<Hex::SIDECOUNT;i++)
        {
            if(data->sides[i]==Hex::color::NONE)
                continue;
            if(data->sides[i]==Hex::color::ANY)
                continue;
            if(data->sides[i]==Hex::color::BLUE)
                color=Qt::GlobalColor::blue;
            if(data->sides[i]==Hex::color::RED)
                color=Qt::GlobalColor::red;
            if(data->sides[i]==Hex::color::YELLOW)
                color=Qt::GlobalColor::yellow;
            if(data->sides[i]==Hex::color::GREEN)
                color=Qt::GlobalColor::green;
            if(data->sides[i]==Hex::color::ERROR)
                color=QColor(255,0,255,255);
            painter->setPen(QPen(QBrush(color),3));

            index1=i;
            index2=(i+1)%Hex::SIDECOUNT;
            painter->drawLine(QPointF(centerX,centerY),
                              QPointF((points[index1]+points[index2])/2));
        }
    }
}
