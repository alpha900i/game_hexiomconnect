#ifndef HEXMAP_H
#define HEXMAP_H

#include <QFile>
#include <QTextStream>
#include <QGraphicsScene>

#include "hex.h"
#include "hexgraphics.h"



class HexMap
{
public:
    //выходной файл и выходной поток для логов
    QFile outputFile;
    QTextStream out;

    //результат проверки: противоречие/пока не решили/решили
    enum checkResult{CONTRADICT=-1,NOTHING=0,SUCCESS=1};

    //пустой гекс, без связей и ограничений.
    //TODO а не сделать ли тебя, братец, паттерном Приспособленец (Flyweight)
    Hex anyHex;
    //карта игрового поля
    Hex **map;
    //перечень доступных гексов
    QVector<Hex> bucket;
    //параметры игрового поля
    int mapWidth,mapHeigth;

    //массивы для получения координат соседей по координатам текущего гекса
    //для четных горизонталей
    int evenShifts[Hex::SIDECOUNT][2]={
        {-2,0},
        {-1,0},
        {1,0},
        {2,0},
        {1,-1},
        {-1,-1}
    };
    //для нечетных горизонталей
    int oddShifts[Hex::SIDECOUNT][2]={
        {-2,0},
        {-1,1},
        {1,1},
        {2,0},
        {1,0},
        {-1,0}
    };

    //конструктор и деструктор
    HexMap();
    ~HexMap();

    //считываем карту поля из файла
    bool readFile(QString fileName);

    //отрисовка главная
    void draw(QGraphicsScene* scene);
    //отрисовка карты
    void drawMap(QGraphicsScene* scene);
    //отрисовка ведра
    void drawBucket(QGraphicsScene* scene);
    //выбираем все незакрепленные узлы в карты и кладем их в ведро
    void cutLoose();
    bool isFit(Hex candidate, int x, int y);
    Hex::color getSideByIndex(int x, int y, int side);
    int checkMap();
    int checkBucket();
    int checkWithRollback();

    int bucketRecursion(int level);
    int mapRecursion(int level);
};

#endif // HEXMAP_H
